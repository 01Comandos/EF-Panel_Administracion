<!DOCTYPE html>
<html lang="es">
  <head>
    <?php include 'inc/head_common.php'; ?>
     
    <script>
      var Dropzone = require("enyo-dropzone");
      Dropzone.autoDiscover = false;
    </script>
  </head>

  <body>
    <?php include 'inc/header.php'; ?>
    
   <section class="add-nueva-empresa">
       <div class="titulo">
           <h1>Nueva Empresa</h1>
       </div>

       <!--DATOS DE FACTURACION-->
        <div class="facturacion-addCompany">
            <form action="inc/agregaEmpresa-formulario.php" method="POST" id="formulario-suscripcion">
              <!-- ZONA DE FACTURACION: AGREGAR EMPRESA NUEVA -->
              <div class="zona-facturacion-suscripcion">
                <span id="titulo-renovacion">Facturación</span>

                <!--campos para la suscripcion-->
                <div class="renovacion-left">
                  <input type="name" name="nombre" placeholder="Nombre Promotor" required="required">   
                  <input type="number" name="factura" placeholder="N° Factura" min="0" max="9999" required="required" >
                </div>

                <div class="renovacion-right">
                    <input type="number" name="factura-total" placeholder="Total Factura" min="1000" max="999999" required="required">
                    <!-- botones de meses -->
                    <div class="btn-group tiempo-suscripcion" data-toggle="buttons">
                      <label class="btn btn-primary active">
                        <input type="radio" name="options" id="option1" autocomplete="off" checked>1 mes
                      </label>
                      <label class="btn btn-primary">
                        <input type="radio" name="options" id="option2" autocomplete="off">2 meses
                      </label>
                      <label class="btn btn-primary">
                        <input type="radio" name="options" id="option3" autocomplete="off">3 meses
                      </label>
                    </div>
                </div>
              </div>

              <!--ZONA DE DATOS EMPRESARIALES -->
              <div class="datos-suscripcion">
                <div class="titulos-secundarios" id="empresa-suscripcion-titulo">
                   <img src="img/generales-icon.png">
                   <span id="datos-empresa-suscripcion">Datos Empresa</span>

                   <!-- Subir archivos -->
                     <div id="actions" class="botones-upload">
                          <!-- The fileinput-button span is used to style the file input field as button -->
                          <!--Upload-logo -->
                          <button class="btn btn-success fileinput-button" id="fileinput-button-Logo">
                            Logotipo
                          </button>

                          <!--Upload imagenes-->
                          <button class="btn btn-success fileinput-button" id="fileinput-button-Galeria">
                            Imágeles Galeria
                          </button>

                          <!-- seleccionar la ciudad -->
                          <div class="ciudad">
                            <div class="btn-group">
                              <button class="btn btn-default btn-sm dropdown-toggle seleccionar-ciudad" type="button" data-toggle="dropdown" aria-expanded="false">
                                Seleccione Ciudad <span class="caret"></span>
                              </button>
                              <ul class="dropdown-menu" role="menu">
                                <li><a href="#">Cumaná</a></li>
                                <li><a href="#">Carupano</a></li>
                                <li><a href="#">Maturín</a></li>
                                <li><a href="#">Pto La Cruz</a></li>
                              </ul>
                            </div>
                          </div>

                        <div class="medidor-subida">
                          <!-- The global file processing state -->
                          <span class="fileupload-process">
                            <div id="total-progress" class="progress progress-striped active" role="progressbar" aria-valuemin="0" aria-valuemax="100" aria-valuenow="0">
                              <div class="progress-bar progress-bar-success" style="width:0%;" data-dz-uploadprogress></div>
                            </div>
                          </span>
                        </div>

                    </div>

                    <!-- espacio donde van a cargarse las imagenes-->
                        <div class="table table-striped" class="files" id="previews">

                          <div id="template" class="file-row">
                            <!-- This is used as the file preview template -->
                            <div>
                                <span class="preview"><img data-dz-thumbnail /></span>
                            </div>
                            <div>
                                <p class="name" data-dz-name></p>
                                <strong class="error text-danger" data-dz-errormessage></strong>
                            </div>
                            <div>
                                <p class="size" data-dz-size></p>
                                <div class="progress progress-striped active" role="progressbar" aria-valuemin="0" aria-valuemax="100" aria-valuenow="0">
                                  <div class="progress-bar progress-bar-success" style="width:0%;" data-dz-uploadprogress></div>
                                </div>
                            </div>
                            <div class="acciones-individuales-suscripcion">
                              <button class="btn btn-primary start">
                                  <i class="glyphicon glyphicon-upload"></i>
                                  <span>Subir</span>
                              </button>
                              <button data-dz-remove class="btn btn-warning cancel">
                                  <i class="glyphicon glyphicon-ban-circle"></i>
                                  <span>Cancelar</span>
                              </button>
                              <button data-dz-remove class="btn btn-danger delete">
                                <i class="glyphicon glyphicon-trash"></i>
                                <span>Borrar</span>
                              </button>
                            </div>
                          </div>
                        </div>

                    <!-- Campos para datos de la empresa -->
                    <div class="datos-empresariales-suscripcion">
                      <div class="renovacion-left">
                        <input type="name" name="nombre" placeholder="Nombre Empresa" required="required">   
                        <input type="name" name="horario1" placeholder="Horario AM" required="required" id="horarioAM-empresa-suscripcion" >
                        <input type="name" name="horario2" placeholder="Horario PM" required="required" id="horarioPM-empresa-suscripcion" >
                        <input type="tel" name="telefono1" placeholder="Teléfono 1" required="required" >
                        <input type="tel" name="telefono2" placeholder="Teléfono 2">
                        <input type="name" name="google-maps" placeholder="Google Maps" required="required" >

                      </div>

                      <div class="renovacion-right">
                          <input type="email" name="email" placeholder="@Email" required="required">
                          <input type="name" name="link-website" placeholder="Link Website">
                          <textarea name="mensaje" cols="30" rows="10" placeholder="Dirección" required="required" id="direccion-empresa-suscripcion"></textarea>
                      </div>
                  </div>

                  <!-- Tipos de pagos que acepta -->
                  <div class="metodos-pago-suscripcion">
                    <div class="titulos-secundarios">
                         <span>Métodos de Pago</span>
                     </div>
                     <div class="btn-group metodos-pago-botones" data-toggle="buttons">
                        <label class="btn btn-primary active" id="maestro-suscripcion">
                          <input type="checkbox" id="option1" autocomplete="off" checked>Maestro
                        </label>
                        <label class="btn btn-primary" id="visa-suscripcion">
                          <input type="checkbox" id="option2" autocomplete="off">Visa
                        </label>
                        <label class="btn btn-primary" id="mastercard-suscripcion">
                          <input type="checkbox" id="option3" autocomplete="off">MasterCard
                        </label>
                    </div>
                  </div>

                  <!-- En el caso de ser Farmacia -->
                  <div class="farmacia-suscripcion">
                    <div class="titulos-secundarios">
                         <span>¿Es una Farmacia?</span>
                     </div>
                    <button class="btn btn-primary btn-farmacia" type="button" data-toggle="collapse" data-target="#farmacia-suscripcion" aria-expanded="false" aria-controls="farmacia-suscripcion"> Calendario Farmacia
                    </button>
                    <div class="collapse" id="farmacia-suscripcion">
                      <div class="well">
                        <form action="#">                          
                          <!-- campo para el calendario -->
                          <input type="date"  min="2015-04-20" name="fecha">
                          <!-- boton para crear un nuevo campo de calendario -->
                          <button id="btn-agregar-calendario">+</button>

                        </form>                        
                      </div>
                    </div>
                  </div>
                  


                  <!--Tipo de publicacion , categoria y estatus -->
                  <!-- tipo de categoria -->
                  <div class="btn-group categoria-suscripcion" data-toggle="buttons">
                    <div class="titulos-secundarios">
                       <span>Categoría</span>
                   </div>
                    <label class="btn btn-primary active" id="generales-suscripcion">
                      <input type="radio" name="options" id="option1" autocomplete="off" checked>Generales
                    </label>
                    <label class="btn btn-primary" id="salud-suscripcion">
                      <input type="radio" name="options" id="option2" autocomplete="off">Salud
                    </label>
                    <label class="btn btn-primary" id="hogar-suscripcion">
                      <input type="radio" name="options" id="option3" autocomplete="off">Hogar
                    </label>
                    <label class="btn btn-primary" id="naturaleza-suscripcion">
                      <input type="radio" name="options" id="option4" autocomplete="off">Naturaleza
                    </label>
                  </div>

                   <!-- tipo de estatus -->
                  <div class="btn-group estatus-suscripcion" data-toggle="buttons">
                    <div class="titulos-secundarios">
                       <span>Estatus</span>
                   </div>
                    <label class="btn btn-primary active" id="sinEstatus-suscripcion">
                      <input type="radio" name="options[]" id="option1" autocomplete="off" checked>Sin Estatus
                    </label>
                    <label class="btn btn-primary" id="principales-suscripcion">
                      <input type="radio" name="options[]" id="option2" autocomplete="off">Principales
                    </label>
                    <label class="btn btn-primary" id="patrocinados-suscripcion">
                      <input type="radio" name="options[]" id="option3" autocomplete="off">Patrocinados
                    </label>
                  </div>

                  <!-- 12 descripciones -->
                  <div class="descripcionesDetalles-suscripcion">
                    <div class="titulos-secundarios">
                       <span>Caracteristicas</span>
                   </div>

                   <!--Grupo1-->
                   <div class="detalles-1-suscripcion">
                     <input type="name" name="descripcion[]" placeholder="Descripción 1" >
                     <input type="name" name="descripcion[]" placeholder="Descripción 2" >
                     <input type="name" name="descripcion[]" placeholder="Descripción 3" >
                     <input type="name" name="descripcion[]" placeholder="Descripción 4" >
                   </div>

                   <!--Grupo2-->
                   <div class="detalles-2-suscripcion">
                     <input type="name" name="descripcion[]" placeholder="Descripción 5" >
                     <input type="name" name="descripcion[]" placeholder="Descripción 6" >
                     <input type="name" name="descripcion[]" placeholder="Descripción 7" >
                     <input type="name" name="descripcion[]" placeholder="Descripción 8" >
                   </div>

                   <!--Grupo3-->
                   <div class="detalles-2-suscripcion">
                     <input type="name" name="descripcion[]" placeholder="Descripción 9" >
                     <input type="name" name="descripcion[]" placeholder="Descripción 10" >
                     <input type="name" name="descripcion[]" placeholder="Descripción 11" >
                     <input type="name" name="descripcion[]" placeholder="Descripción 12" >
                   </div>
                  </div>
              
               </div>
              </div>
                

                <div class="btn-renovacion">
                  <input type="submit" value="Procesar" id="btn-suscripcion-empresa">
                </div>
            </form>  

        </div>
   </section>

   <script>
      jQuery(function() {
          jQuery('.menu-Panel-Administracion a').removeClass('opciones-panel active');
          jQuery('.menu-Panel-Administracion a:nth-of-type(2)').addClass('opciones-panel active');
      })
   </script>

  </body>
  <?php include 'inc/footer_common.php'; ?>
  
</html>