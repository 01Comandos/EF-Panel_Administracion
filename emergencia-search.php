<!DOCTYPE html>
<html lang="es">
  <head>
    <?php include 'inc/head_common.php'; ?>
  </head>
  <body>
    <?php include 'inc/header.php'; ?>
    
   <section class="nueva-empresa">
       <div class="titulo">
           <h1>Emergencias</h1>

           <!--Buscador y boton agregar empresa-->
           <div class="acciones-NE">
             <a href="addnew-emergencia.php" class="add-company">Agregar Emergencia</a>
             <div class="search">
              <input type="search" name="busqueda" placeholder="Buscar" id="buscador">
            </div>
           </div>
       </div>

       <!--Listado de Empresas existentes-->
       <div class="listado-empresas">
        <!-- Emergencia 1-->
          <div class="empresa-cargada">
            <div class="estatus" id="ciudad-estatus">
               <span>Cumaná - PNB</span>
             </div>
             <div class="fecha">
               <span>Agregado el 06/02/2015</span>
             </div>

             <!--datos detallados de la empresa en el listado -->
             <div class="datos-empresa-registrada">
              <a href="#">
                  <figure class="logo-empresa-registrada" id="logo-emergencia" >
                   <img src="img/emergencias/policia.jpg" alt="">
                 </figure>
              </a>
               
               <div class="datos-detalle">
                 <p id="direccion-registrado">Calle Rio Caribe cruce con calle principal. Local 01-A  Sector Boca de Sabana. Cumaná - Estado Sucre. Venezuela</p>
                 <p id="telefono-registrado">(0293) 457.2014</p>
               </div>
               <div class="botones-rapidos">
                 <button class="btn" id="edit-empresa">Editar</button>
                 <a href="#" class="borrar-empresa">Borrar</a>
               </div>
             </div>
          </div>

          <!-- Emergencia 2-->
          <div class="empresa-cargada">
            <div class="estatus" id="ciudad-estatus">
               <span>Cumaná - Bomberos</span>
             </div>
             <div class="fecha">
               <span>Agregado el 06/02/2015</span>
             </div>

             <!--datos detallados de la empresa en el listado -->
             <div class="datos-empresa-registrada">
              <a href="#">
                  <figure class="logo-empresa-registrada" id="logo-emergencia">
                   <img src="img/emergencias/bomberos.jpg" alt="">
                 </figure>
              </a>
               
               <div class="datos-detalle">
                 <p id="direccion-registrado">Calle Rio Caribe cruce con calle principal. Local 01-A  Sector Boca de Sabana. Cumaná - Estado Sucre. Venezuela</p>
                 <p id="telefono-registrado">(0293) 457.2014</p>
               </div>
               <div class="botones-rapidos">
                 <button class="btn" id="edit-empresa">Editar</button>
                 <a href="#" class="borrar-empresa">Borrar</a>
               </div>
             </div>
          </div>

          <!-- Emergencia 3-->
          <div class="empresa-cargada">
            <div class="estatus" id="ciudad-estatus">
               <span>Cumaná - CICPC</span>
             </div>
             <div class="fecha">
               <span>Agregado el 06/02/2015</span>
             </div>

             <!--datos detallados de la empresa en el listado -->
             <div class="datos-empresa-registrada">
              <a href="#">
                  <figure class="logo-empresa-registrada" id="logo-emergencia">
                   <img src="img/emergencias/policia-elite.jpg" alt="">
                 </figure>
              </a>
               
               <div class="datos-detalle">
                 <p id="direccion-registrado">Calle Rio Caribe cruce con calle principal. Local 01-A  Sector Boca de Sabana. Cumaná - Estado Sucre. Venezuela</p>
                 <p id="telefono-registrado">(0293) 457.2014</p>
               </div>
               <div class="botones-rapidos">
                 <button class="btn" id="edit-empresa">Editar</button>
                 <a href="#" class="borrar-empresa">Borrar</a>
               </div>
             </div>
          </div>

          <!-- Emergencia 4-->
          <div class="empresa-cargada">
            <div class="estatus" id="ciudad-estatus">
               <span>Cumaná - Ministerio del Trabajo</span>
             </div>
             <div class="fecha">
               <span>Agregado el 06/02/2015</span>
             </div>

             <!--datos detallados de la empresa en el listado -->
             <div class="datos-empresa-registrada">
              <a href="#">
                  <figure class="logo-empresa-registrada" id="logo-emergencia">
                   <img src="img/emergencias/ministerio.jpg" alt="">
                 </figure>
              </a>
               
               <div class="datos-detalle">
                 <p id="direccion-registrado">Calle Rio Caribe cruce con calle principal. Local 01-A  Sector Boca de Sabana. Cumaná - Estado Sucre. Venezuela</p>
                 <p id="telefono-registrado">(0293) 457.2014</p>
               </div>
               <div class="botones-rapidos">
                 <button class="btn" id="edit-empresa">Editar</button>
                 <a href="#" class="borrar-empresa">Borrar</a>
               </div>
             </div>
          </div>

          <!-- Empresa 5-->
          <div class="empresa-cargada">
            <div class="estatus" id="ciudad-estatus">
               <span>Cumaná - CICPC</span>
             </div>
             <div class="fecha">
               <span>Agregado el 06/02/2015</span>
             </div>

             <!--datos detallados de la empresa en el listado -->
             <div class="datos-empresa-registrada">
              <a href="#">
                  <figure class="logo-empresa-registrada" id="logo-emergencia">
                   <img src="img/emergencias/policia-elite.jpg" alt="">
                 </figure>
              </a>
               
               <div class="datos-detalle">
                 <p id="direccion-registrado">Calle Rio Caribe cruce con calle principal. Local 01-A  Sector Boca de Sabana. Cumaná - Estado Sucre. Venezuela</p>
                 <p id="telefono-registrado">(0293) 457.2014</p>
               </div>
               <div class="botones-rapidos">
                 <button class="btn" id="edit-empresa">Editar</button>
                 <a href="#" class="borrar-empresa">Borrar</a>
               </div>
             </div>
          </div>
       </div>    
   </section>

   <script>
      jQuery(function() {
        jQuery('.menu-Panel-Administracion a').removeClass('opciones-panel active');
        jQuery('.menu-Panel-Administracion a:nth-of-type(3)').addClass('opciones-panel active');
      })
   </script>
   
  </body>
  <?php include 'inc/footer_common.php'; ?>
  
</html>