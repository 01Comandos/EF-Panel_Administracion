<!DOCTYPE html>
<html lang="es">
  <head>
    <?php include 'inc/head_common.php'; ?>
  </head>
  <body>
    <?php include 'inc/header.php'; ?>
    
   <section class="nueva-empresa">
       <div class="titulo">
           <h1>Nueva Empresa</h1>

           <!--Buscador y boton agregar empresa-->
           <div class="acciones-NE">
             <a href="addnew-company.php" class="add-company">Nueva Empresa</a>
             <div class="search">
              <input type="search" name="busqueda" placeholder="Buscar" id="buscador">
            </div>
           </div>
       </div>

       <!--Listado de Empresas existentes-->
       <div class="listado-empresas">
        <!-- Empresa 1-->
          <div class="empresa-cargada">
            <div class="estatus">
               <span class="premium">Premium</span>
               <span class="generales-estatus">Generales</span>
               <span class="principales">Principales</span>
             </div>
             <div class="fecha">
               <span>Agregado el 06/02/2015</span>
             </div>

             <!--datos detallados de la empresa en el listado -->
             <div class="datos-empresa-registrada">
              <a href="#">
                  <figure class="logo-empresa-registrada">
                   <img src="img/empresa-1.jpg" alt="">
                 </figure>
              </a>
               
               <div class="datos-detalle">
                 <p id="direccion-registrado">Calle Rio Caribe cruce con calle principal. Local 01-A  Sector Boca de Sabana. Cumaná - Estado Sucre. Venezuela</p>
                 <p id="telefono-registrado">(0293) 457.2014</p>
                 <p id="email-registrado"><img src="img/email-icon.png" alt="">atencion@tabularisA.com</p>
               </div>
               <div class="botones-rapidos">
                 <button class="btn" id="edit-empresa">Editar</button>
                 <a href="#" class="borrar-empresa">Borrar</a>
               </div>
             </div>
          </div>

          <!-- Empresa 2-->
          <div class="empresa-cargada">
            <div class="estatus">
               <span class="premium">Premium</span>
               <span class="hogar">Hogar</span>
               <span class="principales">Principales</span>
             </div>
             <div class="fecha">
               <span>Agregado el 06/02/2015</span>
             </div>

             <!--datos detallados de la empresa en el listado -->
             <div class="datos-empresa-registrada">
              <a href="#">
                  <figure class="logo-empresa-registrada">
                   <img src="img/empresa-3.jpg" alt="">
                 </figure>
              </a>
               
               <div class="datos-detalle">
                 <p id="direccion-registrado">Calle Rio Caribe cruce con calle principal. Local 01-A  Sector Boca de Sabana. Cumaná - Estado Sucre. Venezuela</p>
                 <p id="telefono-registrado">(0293) 457.2014</p>
                 <p id="email-registrado"><img src="img/email-icon.png" alt="">atencion@tabularisA.com</p>
               </div>
               <div class="botones-rapidos">
                 <button class="btn" id="edit-empresa">Editar</button>
                 <a href="#" class="borrar-empresa">Borrar</a>
               </div>
             </div>
          </div>

          <!-- Empresa 3-->
          <div class="empresa-cargada">
            <div class="estatus">
               <span class="normal">Normal</span>
               <span class="salud">Salud</span>
               <span class="recomendados">Recomendados</span>
             </div>
             <div class="fecha">
               <span>Agregado el 06/02/2015</span>
             </div>

             <!--datos detallados de la empresa en el listado -->
             <div class="datos-empresa-registrada">
              <a href="#">
                  <figure class="logo-empresa-registrada">
                   <img src="img/empresa-4.jpg" alt="">
                 </figure>
              </a>
               
               <div class="datos-detalle">
                 <p id="direccion-registrado">Calle Rio Caribe cruce con calle principal. Local 01-A  Sector Boca de Sabana. Cumaná - Estado Sucre. Venezuela</p>
                 <p id="telefono-registrado">(0293) 457.2014</p>
                 <p id="email-registrado"><img src="img/email-icon.png" alt="">atencion@tabularisA.com</p>
               </div>
               <div class="botones-rapidos">
                 <button class="btn" id="edit-empresa">Editar</button>
                 <a href="#" class="borrar-empresa">Borrar</a>
               </div>
             </div>
          </div>

          <!-- Empresa 4-->
          <div class="empresa-cargada">
            <div class="estatus">
               <span class="normal">Normal</span>
               <span class="naturaleza">Naturaleza</span>
             </div>
             <div class="fecha">
               <span>Agregado el 06/02/2015</span>
             </div>

             <!--datos detallados de la empresa en el listado -->
             <div class="datos-empresa-registrada">
              <a href="#">
                  <figure class="logo-empresa-registrada">
                   <img src="img/empresa-2.jpg" alt="">
                 </figure>
              </a>
               
               <div class="datos-detalle">
                 <p id="direccion-registrado">Calle Rio Caribe cruce con calle principal. Local 01-A  Sector Boca de Sabana. Cumaná - Estado Sucre. Venezuela</p>
                 <p id="telefono-registrado">(0293) 457.2014</p>
                 <p id="email-registrado"><img src="img/email-icon.png" alt="">atencion@tabularisA.com</p>
               </div>
               <div class="botones-rapidos">
                 <button class="btn" id="edit-empresa">Editar</button>
                 <a href="#" class="borrar-empresa">Borrar</a>
               </div>
             </div>
          </div>

          <!-- Empresa 5-->
          <div class="empresa-cargada">
            <div class="estatus">
               <span class="premium">Premium</span>
               <span class="hogar">Hogar</span>
               <span class="principales">Principales</span>
             </div>
             <div class="fecha">
               <span>Agregado el 06/02/2015</span>
             </div>

             <!--datos detallados de la empresa en el listado -->
             <div class="datos-empresa-registrada">
              <a href="#">
                  <figure class="logo-empresa-registrada">
                   <img src="img/empresa-3.jpg" alt="">
                 </figure>
              </a>
               
               <div class="datos-detalle">
                 <p id="direccion-registrado">Calle Rio Caribe cruce con calle principal. Local 01-A  Sector Boca de Sabana. Cumaná - Estado Sucre. Venezuela</p>
                 <p id="telefono-registrado">(0293) 457.2014</p>
                 <p id="email-registrado"><img src="img/email-icon.png" alt="">atencion@tabularisA.com</p>
               </div>
               <div class="botones-rapidos">
                 <button class="btn" id="edit-empresa">Editar</button>
                 <a href="#" class="borrar-empresa">Borrar</a>
               </div>
             </div>
          </div>
       </div>    
   </section>

   <script>
      jQuery(function() {
      jQuery('.menu-Panel-Administracion a').removeClass('opciones-panel active');
      jQuery('.menu-Panel-Administracion a:nth-of-type(2)').addClass('opciones-panel active');
      })
</script>

  </body>
  
  <?php include 'inc/footer_common.php'; ?>
  
</html>