<!DOCTYPE html>
<html lang="es">
  <head>
    <?php include 'inc/head_common.php'; ?>
  </head>
  <body class="login">
  
    <!-- REGISTRO DE ADMINISTRADOR -->
      <div class="container">
          <div class="col -md-12 col-lg-12">
            <form action="inc/registro-formulario.php" method="POST" id="formulario-registro">
                <figure>
                  <img src="img/logotipo.png" alt="">
                </figure>
                <!--usuario -->
                <div class="usuario">
                  <span>Usuario</span>
                  <input type="name" name="nombre" required="required" id="admin" >
                </div>

                <!-- password -->
                <div class="password">
                  <span>Password</span>
                  <input type="password" name="password" required="required" id="password" >
                </div>

                <div class="btn-login">
                  <input type="submit" value="Login" id="btn-login-register">
                </div>

                <span id="regresar-login">Regresar a <a href="http://encuentralofacilito.com/">encuentralofacilito.com</a></span>
            </form>  
          </div>
      </div>
  </body>
  <?php include 'inc/footer_common.php'; ?>
  
</html>