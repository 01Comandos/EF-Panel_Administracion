<!DOCTYPE html>
<html lang="es">
  <head>
    <?php include 'inc/head_common.php'; ?>
  </head>
  <body>
    <?php include 'inc/header.php'; ?>
    
   <section class="generales">
       <div class="titulo">
           <h1>Generales</h1>
       </div>

       <!-- Balance -->
       <div class="total-empresas">
            <img src="img/total-icon.png" alt="">
           <span>324 Empresas</span>
       </div>

       <!--balance detalle -->
        <div class="total-especifico">
            <!--Premium Total-->
            <div class="premium-total">
                <span id="total-tipo-empresa">60</span>
                <span class="premium">Premium</span>
            </div>

            <!--Normal total-->
            <div class="normal-total">
                <span id="total-tipo-empresa">320</span>
                <span class="normal">Normal</span>
            </div>

            <!--Recomendados -->
            <div class="recomendados-total">
                <span id="total-tipo-empresa">40</span>
                <span class="recomendados">Recomendados</span>
            </div>

            <!--Principales -->
            <div class="principales-total">
                <span id="total-tipo-empresa">16</span>
                <span class="principales">Principales</span>
            </div>
        </div>

        <!--ULTIMOS 5 DÍAS -->
        <div class="ultimos-dias">
            <div class="titulos-secundarios">
               <span>Últimos 5 días</span>
           </div>

           <!--detalles -->
           <div class="reporte-empresas">
               <span><strong>30 Empresas</strong> (nuevas)</span>
               <span id="margen"><strong>De alta:</strong> 0 Empresas</span>
           </div>
        </div>

        <!--reporte por categorias -->
        <div class="reporte-categorias">
            <div class="titulos-secundarios">
               <span>Categorías </span>
           </div>

           <!--detalles -->
           <div class="reporte-categorias-detalle">
               <!--Generales -->
                <div class="generales-total">
                    <span class="generales-estatus">Generales</span>
                    <span id="total-tipo-empresa">63 Empresas</span>
                </div>

                <!--Salud -->
                <div class="salud-total">
                    <span class="salud">Salud</span>
                    <span id="total-tipo-empresa">150 Empresas</span>
                </div>

                <!--hogar -->
                <div class="hogar-total">
                    <span class="hogar">Hogar</span>
                    <span id="total-tipo-empresa">54 Empresas</span>
                </div>

                <!--naturaleza -->
                <div class="naturaleza-total">
                    <span class="naturaleza">Naturaleza</span>
                    <span id="total-tipo-empresa">20 Empresas</span>
                </div>
           </div>
        </div>

        <!--PROXIMOS A FECHA DE CORTE -->
        <div class="reporte-fechas-proximas">
            <div class="titulos-secundarios" id="fechas-corte">
               <span>Próximos a Fechas de Corte </span>
           </div>

           <div class="empresas-corte">
                <!-- EMPRESA 1 -->
               <div class="empresa-vencimiento">
                   <div class="acciones">
                        <button class="md-trigger check" data-modal="modal-9"></button>
                       <a href="#" class="negative"></a>
                   </div>
                   <figure class="logo-empresa">
                       <img src="img/empresa-1.jpg" alt="">
                   </figure>

                   <div class="datos">
                       <span id="fecha-corte">15/05/2015</span>
                       <span id="telefono-empresa">(0293) 421.5896</span>
                       <span id="email-empresa">atencion@empresa.com</span>
                   </div>
               </div>

               <!-- EMPRESA 2 -->
               <div class="empresa-vencimiento margen-empresa">
                   <div class="acciones">
                       <button class="md-trigger check" data-modal="modal-9"></button>
                       <a href="#" class="negative"></a>
                   </div>
                   <figure class="logo-empresa">
                       <img src="img/empresa-2.jpg" alt="">
                   </figure>

                   <div class="datos">
                       <span id="fecha-corte">15/05/2015</span>
                       <span id="telefono-empresa">(0293) 421.5896</span>
                       <span id="email-empresa">atencion@empresa.com</span>
                   </div>
               </div>

               <!-- EMPRESA 3 -->
               <div class="empresa-vencimiento margen-empresa">
                   <div class="acciones">
                       <button class="md-trigger check" data-modal="modal-9"></button>
                       <a href="#" class="negative"></a>
                   </div>
                   <figure class="logo-empresa">
                       <img src="img/empresa-3.jpg" alt="">
                   </figure>

                   <div class="datos">
                       <span id="fecha-corte">15/05/2015</span>
                       <span id="telefono-empresa">(0293) 421.5896</span>
                       <span id="email-empresa">atencion@empresa.com</span>
                   </div>
               </div>

               <!-- EMPRESA 4 -->
               <div class="empresa-vencimiento">
                   <div class="acciones">
                       <button class="md-trigger check" data-modal="modal-9"></button>
                       <a href="#" class="negative"></a>
                   </div>
                   <figure class="logo-empresa">
                       <img src="img/empresa-4.jpg" alt="">
                   </figure>

                   <div class="datos">
                       <span id="fecha-corte">15/05/2015</span>
                       <span id="telefono-empresa">(0293) 421.5896</span>
                       <span id="email-empresa">atencion@empresa.com</span>
                   </div>
               </div>

               <!-- EMPRESA 5 -->
               <div class="empresa-vencimiento margen-empresa">
                   <div class="acciones">
                       <button class="md-trigger check" data-modal="modal-9"></button>
                       <a href="#" class="negative"></a>
                   </div>
                   <figure class="logo-empresa">
                       <img src="img/empresa-1.jpg" alt="">
                   </figure>

                   <div class="datos">
                       <span id="fecha-corte">15/05/2015</span>
                       <span id="telefono-empresa">(0293) 421.5896</span>
                       <span id="email-empresa">atencion@empresa.com</span>
                   </div>
               </div>

               <!-- EMPRESA 6 -->
               <div class="empresa-vencimiento margen-empresa">
                   <div class="acciones">
                       <button class="md-trigger check" data-modal="modal-9"></button>
                       <a href="#" class="negative"></a>
                   </div>
                   <figure class="logo-empresa">
                       <img src="img/empresa-3.jpg" alt="">
                   </figure>
                   <div class="datos">
                       <span id="fecha-corte">15/05/2015</span>
                       <span id="telefono-empresa">(0293) 421.5896</span>
                       <span id="email-empresa">atencion@empresa.com</span>
                   </div>
               </div>
           </div>
        </div>

        <!-- VENTANA MODAL -->
        <div class="modal-renovacion md-modal md-effect-9" id="modal-9">
            <div class="md-content">
                <button class="md-close">x</button>
                <form action="inc/renovacion-formulario.php" method="POST" id="formulario-renovacion">
                    <figure class="logo-empresa-renovacion">
                      <img src="img/empresa-1.jpg" alt="">
                    </figure>
                    <span id="titulo-renovacion">Renovación</span>

                    <!--campos para la renovación-->
                    <div class="renovacion-left">
                      <input type="name" name="nombre" placeholder="Nombre Promotor" required="required" >   
                      <input type="number" name="factura" placeholder="N° Factura" min="0" max="9999" required="required">
                    </div>

                    <div class="renovacion-right">
                        <input type="number" name="factura-total" placeholder="Total Factura" min="1000" max="999999" required="required">
                        <!-- botones de meses -->
                        <div class="btn-group tiempo-renovacion" data-toggle="buttons">
                          <label class="btn btn-primary active">
                            <input type="radio" name="options" id="option1" autocomplete="off" checked>1 mes
                          </label>
                          <label class="btn btn-primary">
                            <input type="radio" name="options" id="option2" autocomplete="off">2 meses
                          </label>
                          <label class="btn btn-primary">
                            <input type="radio" name="options" id="option3" autocomplete="off">3 meses
                          </label>
                        </div>
                    </div>

                    <button class="btn btn-primary btn-farmacia" type="button" data-toggle="collapse" data-target="#farmacia-suscripcion" aria-expanded="false" aria-controls="farmacia-suscripcion"> Calendario Farmacia
                    </button>
                    <div class="collapse" id="farmacia-suscripcion">
                      <div class="well">
                        <form action="#">                          
                          <!-- campo para el calendario -->
                          <input type="date"  min="2015-04-20" name="fecha">
                          <!-- boton para crear un nuevo campo de calendario -->
                          <button id="btn-agregar-calendario">+</button>

                        </form>                        
                      </div>
                    </div>

                    <div class="btn-renovacion">
                      <input type="submit" value="Procesar" id="btn-renovacion-empresa">
                    </div>
                </form>  
            </div>
        </div>
        <div class="md-overlay"></div><!-- the overlay element -->

   </section>

   <script>
      jQuery(function() {
      jQuery('.menu-Panel-Administracion a').removeClass('opciones-panel active');
      jQuery('.menu-Panel-Administracion a:nth-of-type(1)').addClass('opciones-panel active');
      })
  </script>

  </body>
  <?php include 'inc/footer_common.php'; ?>
  
</html>