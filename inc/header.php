<header>
    <figure>
        <img src="img/logo-header.png" alt="">
    </figure>
    
    <!--botones - menu -->
    <nav class="menu-Panel-Administracion">
        <!-- Generales -->
        <a href="index.php" class="active" id="index-panel"><img src="img/generales-icon.png" alt="">Generales</a>

         <!-- Empresa Nueva -->
        <a href="add-company.php" id="nueva-empresa"><img src="img/nueva-empresa-icon.png" alt="">Nueva Empresa</a>  

        <!-- Agregar Emergencias -->
        <a href="emergencia-search.php" id="holis">Emergencias</a>    
    </nav>

</header>