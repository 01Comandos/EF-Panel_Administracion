<!DOCTYPE html>
<html lang="es">
  <head>
    <?php include 'inc/head_common.php'; ?>
     
    <script>
      var Dropzone = require("enyo-dropzone");
      Dropzone.autoDiscover = false;
    </script>
  </head>

  <body>
    <?php include 'inc/header.php'; ?>
    
   <section class="add-nueva-empresa">
       <div class="titulo">
           <h1>Nueva Emergencia</h1>
       </div>

       <!--DATOS DE FACTURACION-->
        <div class="facturacion-addCompany">
            <form action="inc/agregaEmpresa-formulario.php" method="POST" id="formulario-emergencia-suscripcion">

              <!--ZONA DE DATOS EMPRESARIALES -->
              <div class="datos-suscripcion">
                <div class="titulos-secundarios" id="empresa-suscripcion-titulo">
                   <span id="datos-empresa-suscripcion">Datos Sitio de Emergencia</span>

                   <!-- Subir archivos -->
                     <div id="actions" class="botones-upload">
                          <!-- The fileinput-button span is used to style the file input field as button -->
                          <!--Upload-logo -->
                          <button class="btn btn-success fileinput-button" id="fileinput-button-avatarEmergencia">
                            Avatar - Emergencia
                          </button>

                          <!-- seleccionar la ciudad -->
                          <div class="ciudad-emergencia">
                            <div class="btn-group">
                              <button class="btn btn-default btn-sm dropdown-toggle seleccionar-ciudad-emergencia" type="button" data-toggle="dropdown" aria-expanded="false">
                                Seleccione Ciudad <span class="caret"></span>
                              </button>
                              <ul class="dropdown-menu" role="menu">
                                <li><a href="#">Cumaná</a></li>
                                <li><a href="#">Carupano</a></li>
                                <li><a href="#">Maturín</a></li>
                                <li><a href="#">Pto La Cruz</a></li>
                              </ul>
                            </div>
                          </div>

                        <div class="medidor-subida">
                          <!-- The global file processing state -->
                          <span class="fileupload-process">
                            <div id="total-progress" class="progress progress-striped active" role="progressbar" aria-valuemin="0" aria-valuemax="100" aria-valuenow="0">
                              <div class="progress-bar progress-bar-success" style="width:0%;" data-dz-uploadprogress></div>
                            </div>
                          </span>
                        </div>

                    </div>

                    <!-- espacio donde van a cargarse las imagenes-->
                        <div class="table table-striped" class="files" id="previews">

                          <div id="template" class="file-row">
                            <!-- This is used as the file preview template -->
                            <div>
                                <span class="preview"><img data-dz-thumbnail /></span>
                            </div>
                            <div>
                                <p class="name" data-dz-name></p>
                                <strong class="error text-danger" data-dz-errormessage></strong>
                            </div>
                            <div>
                                <p class="size" data-dz-size></p>
                                <div class="progress progress-striped active" role="progressbar" aria-valuemin="0" aria-valuemax="100" aria-valuenow="0">
                                  <div class="progress-bar progress-bar-success" style="width:0%;" data-dz-uploadprogress></div>
                                </div>
                            </div>
                            <div class="acciones-individuales-suscripcion">
                              <button class="btn btn-primary start">
                                  <i class="glyphicon glyphicon-upload"></i>
                                  <span>Subir</span>
                              </button>
                              <button data-dz-remove class="btn btn-warning cancel">
                                  <i class="glyphicon glyphicon-ban-circle"></i>
                                  <span>Cancelar</span>
                              </button>
                              <button data-dz-remove class="btn btn-danger delete">
                                <i class="glyphicon glyphicon-trash"></i>
                                <span>Borrar</span>
                              </button>
                            </div>
                          </div>
                        </div>

                    <!-- Campos para datos de la emergencia -->
                    <div class="datos-emergencia-suscripcion">
                      <div class="renovacion-left">
                        <input type="name" name="nombre" placeholder="Nombre Sitio Emergencia" required="required">   
                        <input type="tel" name="telefono1" placeholder="Teléfono 1" required="required" >
                        <input type="name" name="google-maps" placeholder="Google Maps" required="required">
                      </div>

                      <div class="renovacion-right">
                          <textarea name="mensaje" cols="30" rows="10" placeholder="Dirección" required="required" id="direccion-emergencia-suscripcion"></textarea>
                      </div>
                  </div>
               </div>
              </div>
                
                <div class="btn-renovacion">
                  <input type="submit" value="Procesar" id="btn-suscripcion-emergencia">
                </div>
            </form>  
        </div>


   </section>
   <script>
      jQuery(function() {
        jQuery('.menu-Panel-Administracion a').removeClass('opciones-panel active');
        jQuery('.menu-Panel-Administracion a:nth-of-type(3)').addClass('opciones-panel active');
      })
   </script>
  </body>
  <?php include 'inc/footer_common.php'; ?>
  
</html>